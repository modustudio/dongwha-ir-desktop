//lazy img 
$(function(){
	$(".js-lazy-img").each(function(){
		var $this = $(this);
		if ($this.offset().top < $(window).scrollTop() + $(window).height() && $this.offset().top > $(window).scrollTop() - $this.height()) {
			var src = $this.attr("data-src");
			$this.attr("src", src);
		} else {
			var n = function() {
				if ($this.offset().top < $(window).scrollTop() + $(window).height() && $this.offset().top > $(window).scrollTop() - $this.height()) {
					var src = $this.attr("data-src");
					$this.attr("src", src);
					$(window).unbind("scroll", n);
				}
			};
			$(window).bind("scroll", n);
		}
	});
});

//lazy bg 
$(function(){
	$(".js-lazy-bg").each(function(){
		var $this = $(this);
		if ($this.offset().top < $(window).scrollTop() + $(window).height() && $this.offset().top > $(window).scrollTop() - $this.height()) {
			var src = $this.attr("data-src");
			$this.css("background-image","url("+src+")");
		} else {
			var n = function() {
				if ($this.offset().top < $(window).scrollTop() + $(window).height() && $this.offset().top > $(window).scrollTop() - $this.height()) {
					var src = $this.attr("data-src");
					$this.css("background-image","url("+src+")");
					$(window).unbind("scroll", n);
				}
			};
			$(window).bind("scroll", n);
		}
	});
});

// dropDown
$(function(){
	var $dropDown = $("[data-drop-down]");
	var $state = $dropDown.find(".js-state");
	var $list = $dropDown.find(".js-list");
	var $item = $dropDown.find(".js-item");
	var toggoleOpen = "is-open";
	var on = "is-on";
	
	$state.attr("tabindex","0");
	$state.on("click keypress",function(e){
		if ((e.keyCode == 13)||(e.type == "click")) {
			if ($(this).hasClass(toggoleOpen)) {
				$(this).removeClass(toggoleOpen).siblings($list).removeClass(toggoleOpen);
			} else { 
				$(this).addClass(toggoleOpen).siblings($list).addClass(toggoleOpen);
			}
		}
	});
	$item.on("click",function(e){
		if (this.tagName != "A") e.preventDefault();
		if ($state.text() == $(this).parent().siblings(".is-on").text()) {
			$(this).closest($list).siblings($state).text($(this).text());
		}
		$(this).parent().addClass(on);
		$(this).parent().siblings().removeClass(on);
		$(this).closest($list).removeClass(toggoleOpen).siblings($state).removeClass(toggoleOpen);
	});
	$(document).on("click",function(e){
		if (!$(e.target).hasClass("js-state") && $list.hasClass(toggoleOpen) && $(e.target).closest($list).hasClass(toggoleOpen) == false) {
			$list.removeClass(toggoleOpen);
			$state.removeClass(toggoleOpen);
		}
	});	
});

// GNB
$(function(){
	var gnb = "#gnbEvent";
	var gnbTop = $(gnb).offset().top;
	$(gnb).on("mouseenter",function(){
		if (!$(gnb).hasClass("is-over")) {
			$(gnb).addClass("is-over");
			var dimmedTop = gnbTop < $(gnb).offset().top ? 0 : gnbTop
			$("<div class='gnb-dimmed' style='top:"+dimmedTop+"px;'></div>").appendTo($(document.body))
		}
	})
	$(window).on("scroll", function(e) { 
		$(".gnb-dimmed").css("top",gnbTop+"px")
	});
	
	$(gnb).on("mouseleave",function(){
		$(gnb).removeClass("is-over")
		$(".gnb-dimmed").remove();
	});
	$(gnb).find("a").on("focusin",function(){
		$(gnb).trigger("mouseenter");
	});
	$(gnb).find("a").last().on("focusout",function(){
		$(gnb).trigger("mouseleave");
	});
})

// header fixed
$(function(){
	var header = ".header";
	var belt = ".js-header-fixed";
	var beltTop = $(belt).offset().top;
	$(window).on("scroll", function(e) {
		if (($(this).scrollTop() > $(belt).offset().top) && (($(header).height() - $(belt).height()) < ($("body")[0].scrollHeight - $("body").height() - $(header).height()))) {
			$(belt).addClass("is-fixed");
		} else if ($(this).scrollTop() < beltTop) {
			$(belt).removeClass("is-fixed");
		}
	});
})

// tab
var tab = function() {
	if ($("[data-ui-tab='true']").length > 0) {
		$("[data-ui-tab='true']").each(function() {
			var $tab = $(this);
			var hash = window.location.hash;
			var tabArray= [];
			$tab.find("a[href^=#]").each(function(idx){
				tabArray.push($tab.find("a[href^=#]").eq(idx).attr("href"));
			});
			for (var i in tabArray) {
				$(tabArray[i]).removeClass("is-show").addClass("is-hide");
			}
			if (hash && tabArray.indexOf(hash) != -1) {
				sele($tab.find("a[href="+hash+"]"),"is-on","is-on");
				$(hash).removeClass("is-hide").addClass("is-show");
			}  else if ($tab.find("[data-ui-full]").length == 1 ) {
				sele($tab.find("a").eq(0),"is-on","is-on");
				for (var i in tabArray) $(tabArray[i]).removeClass("is-hide").addClass("is-show");
			} else {
				if ($tab.children().hasClass("is-on")) {
					$($tab.children(".is-on").find("a[href^=#]").attr("href")).removeClass("is-hide").addClass("is-show");
				} else {
					sele($tab.find("a[href^=#]").eq(0),"is-on","is-on");
					$(tabArray[0]).removeClass("is-hide").addClass("is-show");
				}
			}
		});
	}
	$(document).on("click","[data-ui-tab='true'] a[href^=#]",function(e){
		e.preventDefault();
		
		var $tab = $(this).closest("[data-ui-tab='true']");
		var hash = window.location.hash;
		var tabArray= [];
		$tab.find("a[href^=#]").each(function(idx){
			tabArray.push($tab.find("a[href^=#]").eq(idx).attr("href"));
		});
		
		
		sele($(this),"is-on","is-on");
		if ($(this).data("ui-full") == true) {
			for (var i in tabArray) $(tabArray[i]).removeClass("is-hide").addClass("is-show");
		} else { 
			if ($tab.data("ui-hash")) {
				if (!$(this).parent().hasClass("is-on")) window.location.hash = ($(this).attr("href"));
			}
			for (var i in tabArray) $(tabArray[i]).removeClass("is-show").addClass("is-hide");
			$($(this).attr("href")).removeClass("is-hide").addClass("is-show");
		}
		
	});
	function sele(el,show,hide) {
		$(el).parent().addClass(show).siblings().removeClass(hide);
	}
};
$(function() {
	tab();
});

// dialog
function dialog(target) {
	if ($(target).hasClass("is-on")) {
		$(target).removeClass("is-on")
		$("[data-target='"+target+"']").remove();
	} else {
		if ($(document.body).find(".dialog-dimmed").length == 0) $("<div class='dialog-dimmed' data-target='"+target+"'></div>").appendTo($(document.body))
		$(target).addClass("is-on")
	}
}

// 글자수 제한
$(function(){
	if ($(".js-check-length-input").length > 0 ) {
		$(".js-check-length-input").each(function(idx){
			$(this).on("keyup", function(){
				var limit = $(this).data("limit");
				var strLength = this.value.length;
				if(strLength > limit){
					this.value=this.value.substring(0,limit);
					this.focus();
					$(".js-check-length-output").eq(idx).html(limit);
				}else{
					$(".js-check-length-output").eq(idx).html(strLength); 
				}
			});
		});
	}
});