/*
npm i -D gulp gulp-connect gulp-watch
*/

var gulp = require('gulp'),
	watch = require('gulp-watch'),
	connect = require('gulp-connect');

// Path
var path = './src';

// Server
gulp.task('serve', function() {
	connect.server({
		root: path,
		port: 8000,
		livereload: true,
		open: {
			browser: 'chrome'
		}
	});
});

gulp.task('taskkill', function() {
  connect.serverClose();
  process.exit();
});

// Watch task
gulp.task('watch',[], function() {

});

gulp.task('default', ['serve']);
